from django.db import models

# Create your models here.


class Company(models.Model):
	title=models.CharField(max_length=200)
	logo=models.ImageField(upload_to="companies")
	description=models.TextField(null=True,blank=True)
	view_count=models.PositiveIntegerField(default=0)

	def __str__(self):
		return self.title





class Laptop(models.Model):
	model_no=models.CharField(max_length=200)
	serial_no=models.CharField(max_length=50,unique=True)
	company=models.ForeignKey(Company,on_delete=models.CASCADE)
	processor=models.CharField(max_length=200)
	description=models.TextField(null=True,blank=True)
	image=models.ImageField(upload_to="laptops")
	view_count=models.PositiveIntegerField(default=0)

	def __str__(self):
		return self.company.title+"-"+self.model_no



class Message(models.Model):
	sender=models.CharField(max_length=200)
	email=models.EmailField(null=True,blank=True)
	mobile=models.CharField(max_length=50)
	subject=models.CharField(max_length=200)
	text=models.TextField()
	date=models.DateTimeField(auto_now_add=True)


	def __str__(self):
		return self.sender