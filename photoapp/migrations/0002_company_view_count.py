# Generated by Django 2.2.8 on 2019-12-23 11:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('photoapp', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='view_count',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
