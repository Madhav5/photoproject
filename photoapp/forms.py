from django import forms

from .models import *


class MyForm(forms.Form):
	name=forms.CharField()
	email=forms.EmailField()
	company=forms.CharField()




class ContactForm(forms.Form):
	fullname=forms.CharField(widget=forms.TextInput(attrs={
		"class":"form-control",

		"placeholder":"Enter your full name",}))
	email=forms.EmailField(required=False,widget=forms.EmailInput(attrs={
		"class":"form-control",

		"placeholder":"Email",}))
	mobile=forms.CharField(widget=forms.NumberInput(attrs={
		"class":"form-control",

		"placeholder":"Phone number",}))
	subject=forms.CharField(widget=forms.TextInput(attrs={
		"class":"form-control",

		"placeholder":"Subject",}))
	message=forms.CharField(widget=forms.Textarea(attrs={
		"class":"form-control",

		"placeholder":"Type your message",
		"rows":5,

		}))





class LaptopForm(forms.ModelForm):
	class Meta:
		model=Laptop
		fields=["model_no",'serial_no','company','processor','description','image']



class RegistrationForm(forms.Form):
	email=forms.EmailField(widget=forms.EmailInput())
	password=forms.CharField(widget=forms.PasswordInput())
	confirm_password=forms.CharField(widget=forms.PasswordInput())


	def clean_confirm_password(self):
		pw=self.cleaned_data['password']
		c_pw=self.cleaned_data['confirm_password']
		if (pw !=c_pw):
			raise forms.ValidationError("Password did not match.")

		if (len(pw)<6):
			raise forms.ValidationError("Password too short")
		return c_pw


	def clean_email(self):
		eml=self.cleaned_data["email"]
		if eml=="abc@gmail.com":
			raise forms.ValidationError("Choose another Email")

		return eml


