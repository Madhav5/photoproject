from django.urls import path
from .views import *


app_name="photoapp"
urlpatterns=[
	path("",HomeView.as_view(),name="home"),
	path("contact/",ContactView.as_view(),name="contact"),
	path("about/",AboutView.as_view(),name="about"),
	path("company/list/",CompanyListView.as_view(),name="companylist"),
	path("company/<int:pk>/detail/",CompanyDetailView.as_view(),name="companydetail"),
	path("laptop/<int:pk>/detail/",LaptopDetailView.as_view(),name="laptopdetail"),
	path("sendmail/",SendEmailView.as_view(),name="sendemail"),
	path("laptop/create/",LaptopCreateView.as_view(),name="laptopcreate"),
	path("register/",RegistrationView.as_view(),name="registration"),
	
	





]