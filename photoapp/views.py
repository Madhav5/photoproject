from django.shortcuts import render

# Create your views here.
from django.views.generic import *
from .models import *
from .forms import *
from django.core.mail import send_mail
from django.conf import settings

class HomeView(TemplateView):
	template_name="home.html"

	def get_context_data(self,**kwargs):
		context=super().get_context_data(**kwargs)
		# context["name"]="Madhav"
		# context["address"]="palpa"
		# context["student"]=["ram","hari","sita"]
		context["alllaptops"]=Laptop.objects.all()
		context["companies"]=Company.objects.all()
		return context



class ContactView(FormView):
	template_name="contact.html"
	form_class=ContactForm
	success_url="/"


	def form_valid(self,form):
		nam=form.cleaned_data["fullname"]
		eml=form.cleaned_data["email"]
		mbl=form.cleaned_data["mobile"]
		sub=form.cleaned_data['subject']
		message=form.cleaned_data['message']
		Message.objects.create(sender=nam,email=eml,mobile=mbl,subject=sub,text=message)

		return super().form_valid(form)



class AboutView(TemplateView):
	template_name="about.html"



class CompanyListView(ListView):
	template_name="companylist.html"
	queryset=Company.objects.all().order_by("-id")  #queryset API
	context_object_name="allcompanies"



class CompanyDetailView(DetailView):
	template_name="companydetail.html"
	queryset=Company.objects.all()     #model=Task
	context_object_name="companyobject"

	def get_context_data(self,**kwargs):
		context=super().get_context_data(**kwargs)
		cmp_id=self.kwargs["pk"]
		this_company=Company.objects.get(id=cmp_id)
		this_company.view_count +=1
		this_company.save()


		return context


class LaptopDetailView(DetailView):
	template_name="laptopdetail.html"
	queryset=Laptop.objects.all()     #model=Task
	context_object_name="laptopobject"

	def get_context_data(self,**kwargs):
		context=super().get_context_data(**kwargs)
		ltp_id=self.kwargs["pk"]
		this_laptop=Laptop.objects.get(id=ltp_id)
		this_laptop.view_count +=1
		this_laptop.save()


		return context



class SendEmailView(FormView):
	template_name="sendmail.html"
	form_class=MyForm
	success_url="/"

	def form_valid(self,form):
		a=form.cleaned_data["name"]
		b=form.cleaned_data["email"]
		c=form.cleaned_data["company"]
		# print(a,b,c)
		send_mail(
		"Quotion",
		"hii " +a+",we have got your message and will contact you soon",
		settings.EMAIL_HOST_USER,
		[b,"sangit.niroula@gmail.com",],
		fail_silently=False
			)
		print("message sent successfully")



		return super().form_valid(form)


class AboutView(TemplateView):
	template_name="about.html"





class LaptopCreateView(CreateView):
	template_name="laptopcreate.html"
	form_class=LaptopForm
	success_url="/"


class RegistrationView(FormView):
	template_name="register.html"
	form_class=RegistrationForm
	success_url="/"






